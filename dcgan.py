from keras.models import Sequential, Model, load_model
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import UpSampling2D
from keras.optimizers import Adam, SGD
from keras.datasets import mnist
from keras.layers import Input, merge, Lambda
from keras.layers.core import Reshape, Dense, Dropout, Activation, Flatten, Layer
from keras.layers.convolutional import Cropping2D, Convolution2D, Deconvolution2D, MaxPooling2D
from keras import backend as K

import numpy as np
import argparse
import math
import random, json, h5py, os, shutil, sys, logging
import tensorflow as tf
from skimage import io, exposure, img_as_uint, img_as_float
from tqdm import trange
from scipy.ndimage.filters import gaussian_filter
from skimage import io

from multi_gpu import make_parallel

def gen_block(input_tensor, input_shape, leaky_relu_alpha=0.2):
    ''' 
    INPUT: (1) input_tensor: block input for functional api
           (2) input_shape: (channels, height, width)
           (3) leaky_relu_alpha: alpha value, default = 0.2
    OUTPUT: (1) block: deconv block
    '''
    channels, height, width = input_shape
    output_shape = (None, channels / 2, height * 2 + 2, width * 2 + 2)

    block = Deconvolution2D(output_shape[1], 4, 4, output_shape=output_shape, 
        border_mode='valid', init='glorot_uniform', subsample=(2,2),)(input_tensor)

    block = Cropping2D(((1,1), (1,1)))(block)
    block = BatchNormalization(mode=2)(block)
    block = LeakyReLU(alpha=leaky_relu_alpha)(block)
    return block

def gen_model(init_size=(5,5), channels=26 * 26, leaky_relu_alpha=0.2):
    '''
    INPUT: (1) init_size: intial width and height of image to generate, default = (5,5)
           (2) channels: num of channels, default = 32 * 32
           (3) leaky_relu_alpha: alpha value, default = 0.2
    OUTPUT: (1) gen: generator model
            (2) gen_input: generator input
            (3) gen_output_shape: generator output shape
    '''
    gen_input = Input(shape=[100])

    init_height, init_width = init_size

    net = Dense(channels * init_height * init_width, init='glorot_normal')(gen_input)
    net = BatchNormalization(mode=2)(net)
    net = LeakyReLU(alpha=leaky_relu_alpha)(net)
    net = Reshape([channels, init_height, init_width])(net)

    net = gen_block(net, (channels, init_height, init_width), leaky_relu_alpha) 
    net = gen_block(net, (channels / 2, init_height * 2, init_width * 2), leaky_relu_alpha) 
    net = gen_block(net, (channels / 4, init_height * 4, init_width * 4), leaky_relu_alpha)

    net = Convolution2D(channels / 26, 5, 5, border_mode='same', init='glorot_uniform')(net)
    net = BatchNormalization(mode=2)(net)
    net = LeakyReLU(leaky_relu_alpha)(net)

    net = Convolution2D(4, 1, 1, border_mode='same', init='glorot_uniform')(net)
    gen_output = Activation('tanh')(net)

    gen = Model(gen_input, gen_output)
    gen_output_shape = (4, init_height * 8, init_width * 8)
    return gen, gen_input, gen_output_shape

def disc_model(input_shape, disc_channels=54, leaky_relu_alpha=0.2):
    '''
    INPUT: (1)
    '''
    disc_input = Input(shape=input_shape)
    
    net = Convolution2D(disc_channels * 4, 5, 5, subsample=(2, 2), border_mode='same')(disc_input)
    net = LeakyReLU(alpha=leaky_relu_alpha)(net)
    
    net = Convolution2D(disc_channels * 6, 3, 3, subsample=(2, 2), border_mode='same')(net)
    net = LeakyReLU(alpha=leaky_relu_alpha)(net)

    net = Convolution2D(disc_channels * 3, 1, 1, border_mode='same')(net)
    net = LeakyReLU(alpha=leaky_relu_alpha)(net)
    net = Flatten()(net)

    net = Dense(disc_channels * 2)(net)
    net = LeakyReLU(alpha=leaky_relu_alpha)(net)
    disc_output = Dense(2, activation='sigmoid')(net)

    disc = Model(disc_input, disc_output)
    return disc, disc_output

def trainable_model(model, mode):
    '''
    INPUT: (1)
    '''
    model.trainable = mode
    for layer in model.layers:
        layer.trainable = mode

def build_models(summary, debug=True, gen_opt=Adam(lr=0.0002, beta_1=0.5), disc_opt=Adam(lr=0.0002, beta_1=0.5), 
    gen_init_size=(5, 5), multi_gpu=False, num_gpu=1):
    '''
    INPUT: (1)
    '''
    gen, gen_input, gen_output_shape = gen_model(gen_init_size)
    gen.compile(loss='binary_crossentropy', optimizer=gen_opt, metrics=['accuracy'])

    input_shape = gen_output_shape
    disc, disc_output = disc_model(input_shape)
    disc.compile(loss='binary_crossentropy', optimizer=disc_opt, metrics=['accuracy'])

    trainable_model(disc, False)

    gan_input = Input(shape=[100])
    net = gen(gan_input)
    gan_output = disc(net)
    
    gan = Model(gan_input, gan_output)
    
    if multi_gpu:
        gan = make_parallel(gan, num_gpu)

    gan.compile(loss='categorical_crossentropy', optimizer=gen_opt, metrics=['accuracy'])

    if debug:
        if(summary == 'gen'):
            print 'Generator summary'
            gen.summary()
        elif(summary == 'disc'):
            print 'Discriminator summary'
            disc.summary()
        elif(summary == 'gan'):
            print 'GAN summary'
            gan.summary()

    return gen, disc, gan, gen_opt, disc_opt

def noise_gen(batch, size=100):
    '''
    INPUT: (1)
    '''
    return np.random.normal(0, 1, size=[batch, 100])

def history_replay(history, batch_size):
    assert batch_size % len(history) == 0

    history_batch = []
    for history_images in history:
        images_count = batch_size / len(history)
        history_batch.append(history_images[np.random.randint(0, history_images.shape[0], size=images_count)])

    history_batch = np.concatenate(history_batch)
    return history_batch

def get_gradients(model):
    '''
    INPUT: (1)
    '''
    # weights = model.trainable_weights
    # weights = [weight for weight in weights if model.get_layer(weight.name[:-2])]
    # return model.optimizer.get_gradients(model.total_loss, weights)
    outputTensor = model.get_output_at(1)
    variableTensorsList = model.trainable_weights
    return K.gradients(outputTensor, variableTensorsList)[0]

def train_disc(disc, history, X_train, batch_size, wgan='None'):
    '''
    INPUT: (1)
    '''
    trainable_model(disc, True)

    # WGAN - clip discriminator weights
    if wgan == 'clip_weight':
        clamp_lower = -0.5
        clamp_upper = 0.5

        for l in disc.layers:
            weights = l.get_weights()
            weights = [np.clip(w, clamp_lower, clamp_upper) for w in weights]
            l.set_weights(weights)
    # WGAN - use gradient penalty
    elif wgan == 'gradient_penalty':
        lambda_param = 10
        gradients = get_gradients(disc)
        
        slopes = tf.sqrt(tf.reduce_mean(tf.square(gradients), reduction_indices=[1]))
        grad_pen = tf.reduce_mean((slopes - 1.0)**2)
        grad_pen += lambda_param * grad_pen
        for l in disc.layers:
            # l.set_weights(l.get_weights() + [grad_pen])
            weights = l.get_weights()
            weights = [tf.add(w, grad_pen) for w in weights]
            l.set_weights(weights)

    X_fake = history_replay(history, batch_size)
    y = np.zeros([batch_size, 2])

    # Soft noisy labels
    y[:, 0] = np.random.uniform(0.7, 1.0, size=[batch_size])
    y[:, 1] = -y[:, 0] + 1.0

    loss_fake, accuracy_fake = disc.train_on_batch(X_fake, y)

    X = X_train[np.random.randint(0, X_train.shape[0], size=batch_size), :, :, :]
    y = np.zeros([batch_size, 2])
    y[:, 1] = np.random.uniform(0.7, 1.2, size=[batch_size])
    y[:, 0] = -y[:, 1] + 1.0

    # Train discriminator
    loss, accuracy = disc.train_on_batch(X, y)
    return loss, accuracy

def train_gen(gan, gen, disc, batch_size):
    '''
    INPUT: (1)
    '''
    gen_loss = 0
    trainable_model(disc, False)
    noise = noise_gen(batch_size)
    y = np.zeros([batch_size, 2])
    y[:, 1] = 1

    loss, accuracy = gan.train_on_batch(noise, y)
    return loss, accuracy

def shake(v_array, rate=0.1):
    return v_array * np.random.uniform(1.0 - rate, 1.0 + rate, size=100)

def create_images(gen, images_count=10, shakes_count=10, noise=None):
    if noise is None:
        noise = noise_gen(images_count)

    input_vectors = []
    for i in range(images_count):
        for j in range(shakes_count):
            original = np.array(noise[i])
            if i > 0:
                shaked = shake(original, 0.3)
            else:
                shaked = original

            input_vectors.append(shaked)

    input_vectors = np.stack(input_vectors)
    gen_images = gen.predict(input_vectors, batch_size=images_count * shakes_count)
    gen_images = (gen_images + 1.0) * 0.5
    return gen_images

def images_filtered(images, num_images):
    '''
    INPUT (1)
    '''
    images_filter = []
    for i in range(len(images)):
        if(i == 0) or (i % num_images == 0):
            images_filter.append(images[i])
    return np.array(images_filter)

def crop_images(images):
    '''
    INPUT (1)
    '''
    cropped_images = []
    for i in xrange(len(images)):
        image = images[i]
        
        slice1 = np.resize(image[0], (41, 41))
        slice2 = np.resize(image[1], (41, 41))
        slice3 = np.resize(image[2], (41, 41))
        slice4 = np.resize(image[3], (41, 41))

        image_resized = np.stack((slice1, slice2, slice3, slice4))

        cropped_images.append(image_resized[:,4:37,4:37]) # Crop patch to 33x33
    return np.array(cropped_images)

def apply_blur(image):
    '''
    INPUT (1)
    '''
    return gaussian_filter(image, 0.15)

def hist_match(input_image, target_image):
    '''
    INPUT (1)
    '''
    num_bins = 255
    input_image_shape = input_image.shape
    input_image = input_image[:,:,np.newaxis]
    target_image = target_image[:,:,np.newaxis]

    img = input_image.copy()

    input_hist, bins = np.histogram(input_image.flatten(), num_bins, normed=True)
    target_hist, bins = np.histogram(target_image.flatten(), num_bins, normed=True)

    input_cdf = input_hist.cumsum()
    input_cdf = (255 * input_cdf / input_cdf[-1]).astype(np.uint8)

    target_cdf = target_hist.cumsum()
    target_cdf = (255 * target_cdf / target_cdf[-1]).astype(np.uint8)

    source_interp = np.interp(input_image.flatten(), bins[:-1], input_cdf)
    target_interp = np.interp(source_interp, target_cdf, bins[:-1])

    img = target_interp.reshape(input_image_shape)
    return img

def train_gan(X_train, gan, disc, gen, tissue_type, modality, hist_images, epoch_count=500000, batch_size=32, render_freq=1000, history_length=8, num_images=10, wgan='None', total_cycles=1):
    '''
    INPUT: (1)
    '''
    gen_image_count = 0
    num_gen = num_images * total_cycles
    gen_image_batch = []
    for cycle in trange(len(range(total_cycles)), desc='cycle loop'):
        fixed_noise = noise_gen(num_images)
        history = []
        for i in range(history_length - 1):
            noise = noise_gen(batch_size)
            gen_images = gen.predict(noise)
            history.append(gen_images)

        for epoch in trange(len(range(epoch_count)), desc='epoch loop', leave=False):
            created_noise = np.random.uniform(0, 1, size=[batch_size, 100])
            gen_images = gen.predict(created_noise)

            # Condition generated images
            if(epoch % 2000 == 0):
                gen_images = apply_blur(gen_images)

            if(epoch % 10000 == 0):
                for image in range(batch_size):
                    img_mod0 = hist_match(gen_images[image][0], hist_images[0])
                    img_mod1 = hist_match(gen_images[image][1], hist_images[1])
                    img_mod2 = hist_match(gen_images[image][2], hist_images[2])
                    img_mod3 = hist_match(gen_images[image][3], hist_images[3])
                    img_comb = np.stack((img_mod0, img_mod1, img_mod2, img_mod3))
                    gen_images[image] = img_comb

            history.append(gen_images)

            disc_loss, disc_accuracy = train_disc(disc, history, X_train, batch_size, wgan)
            gen_loss, gen_accuracy = train_gen(gan, gen, disc, batch_size)

            history.pop(random.choice(range(history_length)))

            if(epoch % render_freq == 0 and epoch != 0):
                disc_stats = 'disc -> loss = {}, accuracy = {}'.format(disc_loss, disc_accuracy)
                gen_stats = 'gen -> loss = {}, accuracy = {}'.format(gen_loss, gen_accuracy)
                print disc_stats
                print gen_stats
                logging.info("\nepoch = " + str(epoch))
                logging.info(disc_stats)
                logging.info(gen_stats)

                images = create_images(gen, images_count=num_images, shakes_count=num_images, noise=fixed_noise)
                #images = crop_images(images)
                images_filter = images_filtered(images, num_images)

                # gen_image_batch.append(images_filter)

                # print 'Saving batch ({}) of generated patches...'.format(batch_size)
                # os.getcwd()
                # os.makedirs('gen_images/epoch{}'.format(epoch))
                # gen_patches_h5 = h5py.File('gen_images/epoch{}/gen_patches_{}_count{}_mod{}.h5'.format(epoch, epoch, 
                #     num_images, modality), 'w')
                # gen_patches_h5.create_dataset('images', data=images_filter)
                # gen_patches_h5.close()
    
        gen_image_batch.append(images_filter)
        gen_image_count += num_images
        print 'Currently {}/{} images generated...'.format(gen_image_count, num_gen)
        print 'Cycle {}/{} completed...'.format(str(cycle + 1), total_cycles)

    print 'Saving generated patches...'
    os.getcwd()
    # os.makedirs('gen_images/batch_size{}'.format(str(num_gen)))
    batch_path = 'gen_images/batch_size2000_1_tissue{}'.format(tissue_type)
    os.makedirs(batch_path)
    # gen_patches_h5 = h5py.File('gen_images/batch_size{}/gen_patches.h5'.format(str(num_gen)), 'w')
    gen_patches_h5 = h5py.File(batch_path + '/gen_patches.h5', 'w')
    gen_patches_h5.create_dataset('images', data=gen_image_batch)
    gen_patches_h5.close()

def save_models(gen, disc, gan, tissue_type, modality):
    ''''
    INPUT: (1)
    '''
    gen.save('saved_models/gen_tissue{}_mod{}.h5'.format(tissue_type, modality))
    disc.save('saved_models/disc_tissue{}_mod{}.h5'.format(tissue_type, modality))
    gan.save('saved_models/gan_tissue{}_mod{}.h5'.format(tissue_type, modality))

def load_models(gen_path, disc_path, gan_path):
    '''
    INPUT: (1)
    '''
    gen = load_model(gen_path)
    disc = load_model(disc_path)
    gan = load_model(gan_path)
    return gen, disc, gan

if __name__ == '__main__':
    # Setup logging
    logging.basicConfig(level=logging.INFO, filename='logs/logfile', filemode='a+', format='%(message)s')

    # Tissue classifications
    healthy_class = 0
    necrotic_core_class = 1
    edema_class = 2
    nonadv_core_class = 3
    adv_core_class = 4 

    # MRI modalities
    flair_mod = 0
    t1_mod = 1
    t1c_mod = 2
    t2 = 3

    tissue_type = adv_core_class
    modality = 'all'

    tissue_class = healthy_class
    x_train_path = 'Original100k_class_' + str(tissue_type)

    gen_path = 'saved_models/gen_tissue{}_mod{}.h5'.format(str(tissue_type), modality)
    disc_path = 'saved_models/disc_tissue{}_mod{}.h5'.format(str(tissue_type), modality)
    gan_path = 'saved_models/gan_tissue{}_mod{}.h5'.format(str(tissue_type), modality)

    def train_gan_model():
        # Load saved patches
        print 'Training GAN model...'
        print 'Loading saved training patches from {}...'.format(x_train_path)
        X_train_h5_file = h5py.File(x_train_path + '/saved_X.h5', 'r')
        X_train = X_train_h5_file['X'][:]
        X_train_h5_file.close() 

        # Load images for histogram matching
        hist_image_mod_0 = io.imread('hist_images/hist_img_mod{}.png'.format(flair_mod))
        hist_image_mod_1 = io.imread('hist_images/hist_img_mod{}.png'.format(t1_mod))
        hist_image_mod_2 = io.imread('hist_images/hist_img_mod{}.png'.format(t1c_mod))
        hist_image_mod_3 = io.imread('hist_images/hist_img_mod{}.png'.format(t2))
        hist_images = [hist_image_mod_0, hist_image_mod_1, hist_image_mod_2, hist_image_mod_3]

        # Check model parameters (options='gen, disc, gan')
        gen, disc, gan, gen_opt, disc_opt = build_models(summary='gen', debug=False, multi_gpu=False, num_gpu=2)

        '''
        wgan options: clip_weight, gradient_penalty, none
        '''
        train_gan(X_train, gan, disc, gen, tissue_type, modality, hist_images, epoch_count=10001, batch_size=32, render_freq=1000, num_images=50, wgan='clip_weight', total_cycles=40)

        print 'Saving models...'
        save_models(gen, disc, gan, tissue_type, modality)

    def generate_more_patches(gen_path, disc_path, gan_path):
        print 'Generating more patches...'
        num_to_gen = 30

        gen, disc, gan = load_models(gen_path, disc_path, gan_path)
        
        created_noise = np.random.uniform(0, 1, size=[num_to_gen, 100])
        gen_images = gen.predict(created_noise)
        print 'gen_images.shape = ', gen_images.shape

        print 'Saving batch of generated patches...'
        gen_patches_h5 = h5py.File('gen_images/more_patches/more_gen_patches.h5', 'w')
        gen_patches_h5.create_dataset('images', data=gen_images)
        gen_patches_h5.close()

    '''
    Start of main 
    '''
    train_gan_model()
    #generate_more_patches(gen_path, disc_path, gan_path)

